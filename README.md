<GRAS> <CHEVALIER>

# Etapes réalisées dans le TP
    - Construction de la classe Calculator
    - Construction de la classe OperationTxt
    - Construction de la classe History
    - Construction de la classe Launcher
    - Test des fonctionalités dans Launcher
    - Améliorations des 3 premières classes
    - Test des fonctionalités dans Launcher
    - Finalisation de la classe Calculator
    - Tests unitaires Junit
    - Packaging avec Jenkins sur branch dev et master
    
# Etapes non réalisées dans le TP
    - Méthodes toJson, toJsonFile, fromJson, fromJsonFile
    
# Répartition des taches
 - Emile CHEVALIER
        - construction des classes
        - construction des tests Junit
        - accompagnement Corentin
        - packaging Jenkins

 - Corentin GRAS
        - Création du repo Gitlab
        - Initialisation du projet maven sur Intellij
        - Compréhension des classes et méthodes
        - Construction de tests Junit
        
# Classes couvertes par des tests unitaires
    - Class Calculator
    - Class OperationTxt
    - Class History
