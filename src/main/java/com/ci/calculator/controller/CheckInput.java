package com.ci.calculator.controller;

public class CheckInput {

    public static boolean checkInteger(String input) {
        int test;
        try {
            Integer.parseInt(input);
            return true;
        }
        catch (NumberFormatException ex) {
            System.out.println("Votre choix n'est pas un nombre.");
            return false;
        }
    }

}
