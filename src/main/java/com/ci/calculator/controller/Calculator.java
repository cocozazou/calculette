package com.ci.calculator.controller;

import com.ci.calculator.model.History;
import com.ci.calculator.model.OperationTxt;

import java.util.ArrayList;
import java.util.HashSet;

public class Calculator {

    private History historique;

    public Calculator() {

        this.historique = new History();
    }

    public Calculator(History historique) {
        this.historique = historique;
    }

    public String GetHistory() {
        return this.historique.getHistoryString();
    }

    public float Add(float a, float b) {
        OperationTxt op = new OperationTxt(String.valueOf(a) + " + "
                + String.valueOf(b) + " = " + (a+b));
        historique.addOperation(op);
        return a + b;
    }

    public float Divide(float a, float b) {
        if (b != 0){
            OperationTxt op = new OperationTxt(String.valueOf(a) + " / "
                    + String.valueOf(b)+ " = " + (a/b));
            historique.addOperation(op);
            return a / b;
        }
        else
            return -1;
    }

    public float Multiply(float a, float b) {
        OperationTxt op = new OperationTxt(String.valueOf(a) + " * "
                + String.valueOf(b)+ " = " + (a*b));
        historique.addOperation(op);
        return a * b;
    }

    public String toBinary(int a) {
        OperationTxt op = new OperationTxt(String.valueOf(a)
                + " = " + String.valueOf(Integer.toBinaryString(a)) + " (Binaire)");
        historique.addOperation(op);
        return Integer.toBinaryString(a);
    }

    public String toHex(int a) {
        OperationTxt op = new OperationTxt(String.valueOf(a)
                + " = " + String.valueOf(Integer.toHexString(a)) + " (Hexadécimal)");
        historique.addOperation(op);
        return Integer.toHexString(a);
    }

    public int factoriel(int a) {
        int result = 1;
        for (int i = 1; i <= a; i++) {
            //System.out.println(i + " " + result + " = " + (i*result));
            result *= i;
        }
        OperationTxt op = new OperationTxt(String.valueOf(a) + "!"
            + " = " + result);
        historique.addOperation(op);
        return result;
    }

    public float firstOrderEquation(float a, float b, float c, float d) {
        float result = 0;
        if ((a-c) != 0) {
            result = (d - b) / (a - c);
            OperationTxt op = new OperationTxt(String.valueOf(a) + "x + " + String.valueOf(b)
                    + " = " + String.valueOf(c) + "x + " + String.valueOf(d) + " | S = "
                    + String.valueOf(result));
            historique.addOperation(op);
            return result;
        }
        else
            return -1;
    }

    public double secondOrderEquation(int a, int b, int c) {
        double espilon = 0.1;
        double delta = Math.pow(b, 2) - (4 * a * c);
        double result = 0;
        if (delta < 0)
            return -1;
        else if (delta <= espilon && delta >= (espilon * (-1))) {
            result = -b / (2*a);
            OperationTxt op = new OperationTxt(String.valueOf(a) + "x² + " + String.valueOf(b)
                    + "x + " + String.valueOf(c) + " = 0" + " | S = " + String.valueOf(result));
            historique.addOperation(op);
        }

        else {
            result = (-b - Math.sqrt(delta));
            result = result / (2*a);
            OperationTxt op = new OperationTxt(String.valueOf(a) + "x² + " + String.valueOf(b)
                    + "x + " + String.valueOf(c) + " = 0" + " | S = " + String.valueOf(result));
            historique.addOperation(op);
        }

        return result;
    }

    public float[] lineEquation(float x0, float y0, float x1, float y1) {
        float[] result = new float[2];
        float coef;
        float ordO;
        if ((x1 - x0) == 0) {
            result[0] = -1;
            result[1] = -1;
            return result;
        }

        else {
            coef = (y1 - y0) / (x1 - x0);
            ordO = y0 - (coef * x0);
            OperationTxt op = new OperationTxt("Points (" + String.valueOf(x0) + ", "
                    + String.valueOf(y0) + ") et (" + String.valueOf(x1) + ", " + String.valueOf(y1)
                    + ") | y = " + String.valueOf(coef) + "x + " + String.valueOf(ordO));
            historique.addOperation(op);
            result[0] = coef;
            result[1] = ordO;
            return result;
        }
    }

    public double distance(float x0, float y0, float x1, float y1) {
        double result = Math.sqrt((double) Math.pow((x1-x0), 2) + Math.pow((y1-y0), 2));
        OperationTxt op = new OperationTxt("Points (" + String.valueOf(x0) + ", "
                + String.valueOf(y0) + ") et (" + String.valueOf(x1) + ", " + String.valueOf(y1)
                + ") | distance = " + String.valueOf(result));
        historique.addOperation(op);
        return result;
    }

}
