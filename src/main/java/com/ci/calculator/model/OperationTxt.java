package com.ci.calculator.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class OperationTxt {

    private String calcul;
    private LocalDate date;

    public OperationTxt(String calcul) {
        this.calcul = calcul;
        this.date = LocalDate.now();
        //History.addOperation();
    }

    public String getCalcul() {

        return this.calcul;
    }

    public LocalDate getDate() {

        return this.date;
    }
}
