package com.ci.calculator.model;

import java.util.ArrayList;

public class History {

    private ArrayList<OperationTxt> history;

    public History() {

        this.history = new ArrayList<>();
        }

    public void addOperation(OperationTxt op) {

        this.history.add(op);
    }

    public ArrayList<OperationTxt> getHistoryOperation() {

        return this.history;
    }

    public String getHistoryString() {
        String result = "";
        for (OperationTxt op : history) {
            result += String.valueOf(op.getDate());
            result += String.format(" : ");
            result += op.getCalcul();
            result += String.format("%n");
        }
        return result;
    }

    public String getHistoryNumber(int index) {
        return this.history.get(index).getCalcul();
    }


}
