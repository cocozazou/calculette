package com.ci.calculator;

import com.ci.calculator.controller.Calculator;
import com.ci.calculator.controller.CheckInput;
import com.ci.calculator.model.History;

import java.util.Scanner;

public class Launcher {

    public static void main(String[] args) {
    History historic = new History();
    Calculator calc = new Calculator(historic);

    calc.Add(5, 3);
    calc.toBinary(42);
    calc.toHex(31);
    calc.toHex(32);
    calc.factoriel(5);
    calc.firstOrderEquation(2, 3, 4, -5);
    calc.secondOrderEquation(1, 4, -21);
    calc.lineEquation(0, 2, 4, 6);
    calc.distance(0, 2, 4, 5);
    System.out.println(calc.GetHistory());
    }
}
