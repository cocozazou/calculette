package test;

import com.ci.calculator.controller.Calculator;
import com.ci.calculator.model.History;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    History historic = new History();
    private Calculator calc = new Calculator(historic);
    @Test
    void getHistory() {
    }

    @Test
    void add() {
        assertEquals(8, calc.Add(5, 3));
    }

    @Test
    void divide() {
        assertEquals(2, calc.Divide(6, 3));
        assertEquals(2.5, calc.Divide(5, 2));
        assertEquals(-1, calc.Divide(5, 0));
    }

    @Test
    void multiply() {
        assertEquals(18, calc.Multiply(6, 3));
        assertEquals(-12, calc.Multiply(-4, 3));
        assertEquals(0, calc.Multiply(6, 0));
    }

    @Test
    void toBinary() {
        assertEquals("101010", calc.toBinary(42));
        assertEquals("0", calc.toBinary(0));
    }

    @Test
    void toHex() {
        assertEquals("1f", calc.toHex(31));
        assertEquals("20", calc.toHex(32));
    }

    @Test
    void factoriel() {
        assertEquals(1, calc.factoriel(0));
        assertEquals(1, calc.factoriel(1));
        assertEquals(120, calc.factoriel(5));
    }

    @Test
    void firstOrderEquation() {
        assertEquals(-1, calc.firstOrderEquation(1, 2, 1, 3));
        assertEquals(4, calc.firstOrderEquation(2, 3, 4, -5));
    }

    @Test
    void secondOrderEquation() {
        assertEquals(-1, calc.secondOrderEquation(3, 2, 10));
        assertEquals(-1, calc.secondOrderEquation(2, 4, 2));
        assertEquals(-7, calc.secondOrderEquation(1, 4, -21));

    }

    @Test
    void lineEquation() {
        float[] test1 = {-1, -1};
        float[] test2 = {1, 2};
        assertEquals(test1[0], calc.lineEquation(1, 2, 1, 5)[0]);
        assertEquals(test1[1], calc.lineEquation(1, 2, 1, 5)[1]);
        assertEquals(test2[0], calc.lineEquation(0, 2, 4, 6)[0]);
        assertEquals(test2[1], calc.lineEquation(0, 2, 4, 6)[1]);
    }

    @Test
    void distance() {
        assertEquals(0, calc.distance(0, 0, 0, 0));
        assertEquals(5, calc.distance(0, 2, 4, 5));
    }
}