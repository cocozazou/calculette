package test;

import com.ci.calculator.model.OperationTxt;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class OperationTxtTest {

    OperationTxt test = new OperationTxt("test");
    @Test
    void getCalcul() {
        assertEquals("test", test.getCalcul());
    }

    @Test
    void getDate() {
        LocalDate date = LocalDate.now();
        assertEquals(date, test.getDate());
    }
}