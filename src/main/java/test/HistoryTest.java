package test;

import com.ci.calculator.controller.Calculator;
import com.ci.calculator.model.History;
import com.ci.calculator.model.OperationTxt;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HistoryTest {

    History historic = new History();

    @Test
    void addOperation() {
        OperationTxt op = new OperationTxt("test calcul");
        historic.addOperation(op);
        assertEquals("test calcul", historic.getHistoryNumber(0));
    }

    @Test
    void getHistoryOperation() {
        ArrayList<OperationTxt> test = new ArrayList<>();
        OperationTxt op = new OperationTxt("test calcul");
        test.add(op);
        historic.addOperation(op);
        assertEquals(test, historic.getHistoryOperation());
    }

    @Test
    void getHistoryString() {
        Calculator calc = new Calculator(historic);
        calc.factoriel(5);
        LocalDate date = java.time.LocalDate.now();
        String test = String.format(String.valueOf(date) + " : 5! = 120%n");
        System.out.println(historic.getHistoryString());
        assertEquals(test, historic.getHistoryString());
    }
}